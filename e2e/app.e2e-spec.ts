import { ComponentStylingPage } from './app.po';

describe('component-styling App', () => {
  let page: ComponentStylingPage;

  beforeEach(() => {
    page = new ComponentStylingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

import {Component} from '@angular/core';
import {MessageOverlayDataModel} from './message-overlay/model/message-overlay-data.model';
import {TextImageBoxDataModel} from './text-image-box/models/text-image-box-data.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Skorebee';
  messageToggleAnimationState = 'off';
  messages = [];
  items: TextImageBoxDataModel[] = [
    {
      name: 'Jonh Doe',
      date: new Date(),
      smallImgUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      largeImageUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit,
  sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
      social: {
        likes: {
          points: 13,
          count: 13
        },
        comments: {
          points: 6,
          count: 3
        },
        shares: {
          points: 0,
          count: 0
        }
      }
    },
    {
      name: 'Jonh Doe',
      date: new Date(),
      smallImgUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      largeImageUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit,
  sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
      social: {
        likes: {
          points: 13,
          count: 13
        },
        comments: {
          points: 6,
          count: 3
        },
        shares: {
          points: 0,
          count: 0
        }
      }
    },
    {
      name: 'Jonh Doe',
      date: new Date(),
      smallImgUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      largeImageUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit,
  sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
      social: {
        likes: {
          points: 13,
          count: 13
        },
        comments: {
          points: 6,
          count: 3
        },
        shares: {
          points: 0,
          count: 0
        }
      }
    }
  ];

  public doChallenge() {
    this.messageToggleAnimationState = 'on';

    this.messages.push({
      increment: Math.round(Math.random() * 10),
      percentage: Math.round(Math.random() * 100)
    });

    setTimeout(() => {
      this.messages.shift();
      // this.messages = this.messages;
      /*this.showMessage = false;*/
    }, 5000);

  }
}

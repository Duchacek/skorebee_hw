import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageOverlayComponent } from './message-overlay.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('MessageOverlayComponent', () => {
  let component: MessageOverlayComponent;
  let fixture: ComponentFixture<MessageOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageOverlayComponent ],
      imports: [
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageOverlayComponent);
    component = fixture.componentInstance;
    component.data.text = 'Hello';
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('has message text "Hello"', () => {
    expect(component.data.text).toBe('Hello');
  });
});

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {MessageOverlayDataModel} from './model/message-overlay-data.model';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-message-overlay',
  templateUrl: './message-overlay.component.html',
  styleUrls: ['./message-overlay.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('messageToggle', [
      state('on', style({opacity: 1})),
      transition('void => on', [
        style({
          opacity: 0
        }),
        animate('1s ease-in-out', style({
          opacity: 1
        }))
      ]),
      transition('* => void', [
        style({
          opacity: 1
        }),
        animate('1s ease-in-out', style({
          opacity: 0
        }))
      ])
    ])
  ]
})
export class MessageOverlayComponent {
  @Input() messageToggleAnimationState;
  @Input() data: MessageOverlayDataModel = {
    increment: null,
    percentage: null
  };
}

export interface MessageOverlayDataModel {
  increment: number;
  percentage: number;
}

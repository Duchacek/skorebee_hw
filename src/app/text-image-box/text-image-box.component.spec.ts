import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TextImageBoxComponent} from './text-image-box.component';

describe('TextImageBoxComponent', () => {
  let component: TextImageBoxComponent;
  let fixture: ComponentFixture<TextImageBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TextImageBoxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextImageBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.data = {
      name: 'John Doe',
      date: new Date(),
      smallImgUrl: '',
      largeImageUrl: '',
      text: `Hello`,
      social: null
    };
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have name John Doe', () => {
    expect(component.data.name).toEqual('John Doe');
  });
});

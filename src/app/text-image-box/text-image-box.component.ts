import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {TextImageBoxDataModel} from './models/text-image-box-data.model';

@Component({
  selector: 'app-text-image-box',
  templateUrl: './text-image-box.component.html',
  styleUrls: ['./text-image-box.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextImageBoxComponent {
  @Input() data: TextImageBoxDataModel;
  @Input() isActive = false;

  constructor() {
    this.data =  {
      name: 'Jonh Doe',
      date: new Date(),
      smallImgUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      largeImageUrl: 'https://cdn3.iconfinder.com/data/icons/internet-and-web-4/78/internt_web_technology-13-512.png',
      text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit,
  sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
      social: {
        likes: {
          points: 13,
          count: 13
        },
        comments: {
          points: 6,
          count: 3
        },
        shares: {
          points: 0,
          count: 0
        }
      }
    };
  }
}
